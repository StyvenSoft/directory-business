﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RegistroPersonas
{
    class Directorio
    {
        public Persona[] datos;
        public int registrados;
        public int cantidad;         // Métodos        
        public Directorio() // Método constructor        
        {
            registrados = 0;
            cantidad = 50;
            datos = new Persona[cantidad];
        }
        public void agregarDatos()
        {
            int i, j; //variables para ciclos for 
            bool encontrado = false; //variable para controlar si existe o no una identificación 
            string linea;
            Console.WriteLine("\t\tDIRECTORIO EMPRESARIAL");
            Console.Write("\nDigite cantidad de datos a registrar: ");
            linea = Console.ReadLine();
            registrados = int.Parse(linea); //almacena el número de personas a registrar 
            for (i = 0; i < registrados; i++) //ciclo para inicializar el arreglo 
            {
                datos[i] = new Persona();
                datos[i].iden = 0;

            }
            Persona persona = new Persona();
            for (i = 0; i < registrados; i++)
            {
                encontrado = false;
                persona.ingresarIdentificacion();
                for (j = 0; j < registrados; j++)
                {
                    if (persona.iden == datos[j].iden)
                    {
                        encontrado = true;
                        i--;
                        break;
                    }
                }
                if (encontrado == true)
                {
                    Console.WriteLine("La persona ya está registrada, ingrese nuevos datos.");
                }
                else
                {
                    persona.ingresarDatos();
                    datos[i].iden = persona.iden;
                    datos[i].nombre = persona.nombre;
                    datos[i].edad = persona.edad;
                    Console.WriteLine("Datos registrado correctamente.");
                }
            }
            Console.ReadKey();
        }
        public void imprimirDatos()
        {
            int i;
            Console.WriteLine("\n\n\t\tIMPRESIÓN DE DIRECTORIO\n");
            for (i = 0; i < registrados; i++)
            {
                Console.WriteLine(datos[i].iden + "   " + datos[i].nombre + "   " + datos[i].edad);

            }
            Console.ReadKey();
        }
    }
}
