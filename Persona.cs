﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RegistroPersonas
{
    class Persona
    {
        public int iden;
        public string nombre;
        public int edad;

        public Persona()
        {
            iden = 0; nombre = ""; edad = 0;
        }
        public void ingresarIdentificacion()
        {
            string linea;
            Console.Write("\nDigite identificación: ");
            linea = Console.ReadLine();
            iden = int.Parse(linea);
        }
        public void ingresarDatos()
        {
            Console.Write("Digite nombre: ");
            nombre = Console.ReadLine();
            Console.Write("Digite edad: ");
            edad = int.Parse(Console.ReadLine());
            while (edad < 0)
            {
                Console.WriteLine("\tEdad incorrecta, el número debe ser positivo");
                Console.Write("\tDigite nuevamente la edad: ");
                edad = int.Parse(Console.ReadLine());
            }
        }

    }
}
