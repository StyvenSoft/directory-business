﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RegistroPersonas
{
    class Program
    {
        static void Main(string[] args)
        {
            Directorio direc1 = new Directorio();
            direc1.agregarDatos(); //llamada al método          
            Console.Write("\n\n\t\tDigite una tecla para continuar");
            Console.ReadKey();
            direc1.imprimirDatos(); //llamada al método   
            Console.Write("\n\n\t\tPrograma finalizado ... digite una tecla para salir");
            Console.ReadKey();
        }
    }
}
